import discord #biblioteca do discord

#outras bibliotecas para coisas especificas
import requests
from PIL import Image, ImageDraw, ImageFont
from random import *
import keep_alive
from vacina import *
from lero_lero import *
import os
os.system("pip install DuckDuckGoImages")
import DuckDuckGoImages as ddg
import glob
import io
import re

#cria o cliente do discord
client = discord.Client()

# o codigo daqui pra frente vai ter um monte de coisa
# que tu n precisa realmente utilizar pra fazer um bot
# ent eu aconselho tu começar a ler onde ta escrito
# INICIO DO CODIGO DO BOT
 

# pode ignorar essas coisas ate achar o INICIO DO CODIGO DO BOT
global trocas_msg
trocas_msg = "souza"

global sorteio
sorteio = ""

def change_sorteio(inp):
  global sorteio
  sorteio += inp + " "
def sorteio_clear():
  global sorteio
  sorteio = ""

def change_trocas(inp):
  global trocas_msg
  trocas_msg = inp

def bot(p1,p2,p3,img_query):
    BASE = 'https://render-tron.appspot.com/screenshot/'
    # url = 'https://lucaspb710.gitlab.io/BotFrases/%3Fa%3D'+p1+'%26b%3D'+p2+'%26c%3D'+p3+'%26c%3D'+img_query
    url = "https://lucaspb710.gitlab.io/BotFrases/%3Fa%3D"+p1+"%26b%3D%0A"+p2+"%26c%3D%0A"+p3+"%26img%3D"+img_query
    path = 'target.jpg'
    response = requests.get(BASE + url, stream=True)
    # save file, see https://stackoverflow.com/a/13137873/7665691
    if response.status_code == 200:
        with open(path, 'wb') as file:
            for chunk in response:
                file.write(chunk)
    print(url)

help_msg = '''
\t\t**Trombas is here to help you!**

**trombas imagem**          Gera uma imagem aleatoria do bot de imagens aleatorias
                            Digita trombas help imagem para mais informações
**trombas lol**             Lol, i poop money
**trombas rot [mensagem]** Gera uma mensagem ~~biblica~~ no estilo rotmg
**trombas me devolve os meus kakera** trombas irá devolver seus kakeras
**trombas f**              trombos f
**trombas wish**           trombas wish to be noticed by mudae
**trombas leilao**         o gato...
**trombas morried**        trombas morried
**trombas sus**            trombas de palhasada
**trombas links**          alguns links importantes
**trombas trombofobia**    mede sua trombofobia
**trombas trombossexualidade** mede sua trombossexualidade
**trombas help_musica**      mostra os comandos pra usar musica no trombas
**trombas sorteio add**      adiciona alguem à lista de sorteio
**trombas sorteio list**     mostra as pessoas na lista de sorteio
**trombas sortear**          sorteia as pessoas na lista aleatoriamente
**trombas sorteio clear**    Limpa a lista de sorteio
**trombas phineas**          Trombas manda uma bela foto do phineas
**trombas fnaf**             procura algo relacionado a FNAF
**trombas genshin/ganso**    procura algo relacionado a genshin impact
**trombas search**           procura o que voce pedir
**fnaf trombas**             manda o link do joguin
**trombas morbius**          pega uma frase aleatoria do roteiro de morbius
**trombas morbius text**     substitui a frase aleatoria pelo texto que voce quiser

**trombas apagar id_da_mensagem** clique com o botão direito e copie o id da mensagem que voce quer apagar
'''

def morbius(texto):
  print(texto)
  fnt = ImageFont.truetype("./Cute.ttf", 120)
  img_ = Image.open("morbius.png")
  image = ImageDraw.Draw(img_)
  image.text((300,400), '\'\''+texto+'*\'\'\n\n-Morbius*', font=fnt, fill=(255,255,255))
  img_.save("Morbius_text.png")


def rotmg(usuario, texto):
#    img = Image.new("RGB", (200,60), color=(80,80,80))
    dungeons = [1,2,3]
    shuffle(dungeons)
    if dungeons[0] == 1:
        img = Image.open("pcave.png")
    elif dungeons[0] == 2:
        img = Image.open("ocastle.png")
    elif dungeons[0] == 3:
        img = Image.open("o3.png")

    fnt = ImageFont.truetype('./Myriad.otf', 14)
    d = ImageDraw.Draw(img)
    #espaco = ""
    #for i in usuario:
     #   espaco += " "
    #espaco += "  "
    #d.text((5,70), usuario, font=fnt, fill=(255,255,0))
    #d.text((len(usuario)*7,70), texto, font=fnt, fill=(255,255,255))
    x = 0
    y = 0
    for char in usuario:
        #print("")
        #print(char)
        #print(fnt.getmask(char).size)
        x+=fnt.getmask(char).size[0]

    #d.text((len(usuario)*7-(len(usuario)*2)+14,71), texto, font = fnt, fill=(0,0,0))
    d.text((x,71), texto, font = fnt, fill=(0,0,0))

    d.text((6,71), usuario, font=fnt, fill=(0,0,0))
    d.text((5,70), usuario, font=fnt, fill=(255,255,0))
    d.text((x,70), texto, font = fnt, fill=(255,255,255))
    img.save('lel_rot.png')



#=========================
# INICIO DO CODIGO DO BOT
#=========================


#diz para o programa analisar qualquer mensagem que seja enviada
@client.event
async def on_message(message):

    #pega a mensagem e transforma todas as suas letras
    #em letras minusculas
    #isso faz com que varias variações como, Trombas links, trombas Links, trombas LinKs, sejam validas
    #ja que tudo isso em letras minusculas seriam "trombas links"
    msg_original = message.content
    msg = message.content.lower()
    message.content = message.content.lower()


    # caso a mensagem seja igual a "trombas links", o trombas
    # irá responder com varios links importantes 
    if 'trombas links' == message.content:
        await message.channel.send("Link para usar o bot: https://lucaspb710.gitlab.io/BotFrases/info.html")
        await message.channel.send("Link para modificar as frases geradas: https://lucaspb710.gitlab.io/BotFrases/gerar")
        await message.channel.send("Link do codigo fonte: https://gitlab.com/LucasPB710/trombas-bot")
        await message.channel.send("Link do fnaf trombas https://gamejolt.com/games/FNAT/700393")

    # comando explicando como o comando
    # "trombas imagem" funciona
    elif 'trombas help imagem' == message.content:
      await message.channel.send("o comando trombas imagem gera uma imagem com frase aleatoria aletoria\nvoce pode escolher qual palavra ficará em qual parte da frase aleatoria usando os seguintes comandos\ntrombas imagem 1 coelho -> fará com que coelho seja a primeira parte da frase\ntrombas imagem 2 coma -> faz com que a o verbo coma seja a segunda parte da frase\ntrombas imagem 3 renato -> faz com que renato seja a ultima parte da frase\n trombas imagem 1 trombas 2 is 3 here to help you -> gera uma imagem aleatoria com a frase trombas is here to help you \n use img Alguma_coisa para que o trombas pesquise o que vier depois de img e coloque como foto de fundo, use _ no lugar de espaços \n")


    # comando que irá pegar uma imagem gerada por um site
    # de acordo com os parametros indicados pelo usuario
    # o uso do message.content.split()[0]+" "+message.content.split()[1]
    # é porque podem vir outras palavras depois de "trombas imagem", como, "trombas imagem 1 passaro"
    # então a gente só verifica as duas primeiras palavras, separando a mensagem por palavras e 
    # pegando a primeira palavra (message.content.split()[0]) e juntando com a segunda (...[1])
    if 'trombas imagem' in message.content.split()[0]+" "+message.content.split()[1]:
        await message.channel.send("imagem sendo gerada...")

        message.content+=" ;"
        msg_s = message.content.split()
        p1 = ""
        p2 = ""
        p3 = ""
        img_query = ""

        has_img  = False

        for i in range(len(msg_s)):
            print(msg_s[i]+"!!!!!!")
            if msg_s[i] == '1':
                i+=1
                while msg_s[i] != '2' and msg_s[i] != '3' and msg_s[i] != ';' and msg_s[i] != "img":
                    p1 += msg_s[i]+"%20"
                    i+=1
                i-=1
                print("p1: "+p1)

            elif msg_s[i] == '2':
                i+=1
                while msg_s[i] != '1' and msg_s[i] != '3' and msg_s[i] != ';' and msg_s[i] != "img":
                    p2 += msg_s[i]+"%20"
                    i+=1
                i-=1
                print("p2: "+p2)

            elif msg_s[i] == '3':
                i+=1
                while msg_s[i] != '1' and msg_s[i] != '2' and msg_s[i] != ';' and msg_s[i] != "img":
                    p3 += msg_s[i]+"%20"
                    i+=1
                i-=1
                print("p3: "+p3)
            elif msg_s[i] == "img": 
                i+=1
                print("looool")
                while msg_s[i] != '1' and msg_s[i] != '2' and msg_s[i] != ';' and msg_s[i] != "img":
                    img_query += msg_s[i]+" "
                    print(img_query)
                    i+=1
                i-=1
                img_urls = ddg.get_image_urls(img_query, None)
                shuffle(img_urls)
                print("SUSSSSS: " + img_urls[0])
                print(img_query)
                has_img = True
            elif msg_s[i] == "url":
                i+=1
                img_urls = [msg_s[i]]  
                i+=1
                has_img= True


        if has_img:
          bot(p1,p2,p3,img_urls[0])
        else:
          bot(p1,p2,p3,"") 

        with open('target.jpg', 'rb') as fp:
            imagem = discord.File(fp, filename="target.jpg", spoiler=False);
        #await message.channel.send(imagem)
        embed = discord.Embed()
        embed.set_image(url="attachment://target.jpg")
        await message.channel.send(file=imagem, embed=embed)


    # caso a mensagem seja "trombas help me"
    # ele envia a string help_msg
    # essa string foi definida na parte de cima do código
    elif 'trombas help me' in message.content:
        await message.channel.send(help_msg)

    #o resto é basicamente a mesma coisa, então se tu quiser tu pode ir olhando comando
    #por comando, mas a proxima coisa importante para fazer um bot está na ultima linha
    # do codigo
    
    elif 'trombas lol' == message.content:
        await message.channel.send("lol, i poop money")
    
    elif 'trombas rot' in message.content.split()[0]+" "+message.content.split()[1]:
        msg_ext = message.content.split()
        msg_ext[0] = ""
        msg_ext[1] = ""
        new_msg = ""
        for i in msg_ext:
            new_msg += i+" "
        if message.author.nick is None:
            usuario = "["+message.author.name+"]: "
        else:
            usuario = "["+message.author.nick+"]: "
        texto = new_msg
        rotmg(usuario, texto)
        with open('lel_rot.png', 'rb') as fp:
            imagem = discord.File(fp, filename='lel_rotmg.png', spoiler=False)
        await message.channel.send(file=imagem)
    
    elif 'trombas escrever' in message.content:
        f = open("trombas.txt", "a")
        f.write('\n'+message.content)
        f.close()
#    elif 'trombas regras' in message.content.split()[0] +" "+message.content.split()[1]:
#      if message.content.split()[2] == "add":
#        regra = ""
#        i = 3
#        while i<len(message.content.split()):
#          regra+=message.content.split()[i]+ " "
#          i+=1
#        f = open ('regras.txt', 'a+')
#        f.write('\n'+regra)
#        f.close()
#        await message.channel.send("regra adicionada!")
#      else:
#        f = open("regras.txt","r")
#        await message.channel.send(f.readlines())


    elif 'trombas me devolve os meus kakera' == message.content:
        await message.channel.send("devolvo nao loooool")
    elif 'trombas f' == message.content:
        await message.channel.send("trombos f")
#        await message.channel.send("$givek 2000 <@310588502016524288>")
    elif 'trombas wish' == message.content:
        await message.channel.send("$wish tom (AC)")
    elif 'trombas leilao' == message.content:
        await message.channel.send("Esse gato possui 10 kakera amais do que foi LEILOADO por maski")
    elif 'trombas ma' == message.content:
        await message.channel.send('$ma')
    elif 'trombas morried' == message.content:
        await message.channel.send("EU VO MORRIED[")
    elif 'TROMBAS' == message.content:
        await message.channel.send("os canibalistas são uma tribo urbana?")
    elif "trombas y" == message.content:
        await message.channel.send("y");
    elif "trombas exit" == message.content:
        await message.channel.send("$exit");

    elif "trombas ban" == message.content:
        await message.channel.send("+BAN <@542104380783198218>")
    elif "trombas divorce" == message.content:
        await message.channel.send("$divorceall")
    elif "trombas dk" == message.content:
        await message.channel.send("$fm Satsuki Kusakabe")
    elif "trombas casar" in message.content:
        #emojis = [':v:', 'emoji_2', 'emoji 3']
        #adminBug = bot.get_channel(message.channel.id)
        #msg = await adminBug.send(embed=embed)
        msg = await message.channel.fetch_message(int(message.content.split()[2]))
        await msg.add_reaction('✌️')
    elif "trombas sus" == message.content:
        await message.channel.send("https://media.discordapp.net/attachments/818231549987520533/875771205058166814/image0.png")
    elif "trombas mine" in message.content:
        await message.channel.send("Mine.")

    elif "trombas souza" == message.content:
          await message.channel.send(trocas_msg)
    elif "trombas trocas" == message.content.split()[0]+" "+message.content.split()[1]:
      change_trocas(message.content.split()[2])
      await message.channel.send("personagem para trocar: "+trocas_msg )

    elif "trombas say" == message.content.split()[0] + " "+ message.content.split()[1]:
      #await message.channel.send("vao sifude vo falar mais nao")
      if ("kakera" in message.content.split()[2] or "kak" in message.content.split()[2]):
        await message.channel.send("ei po deixe de palhasada ae")
      elif len(message.content.split()) >= 4 and "trombas say" == message.content.split()[2] + " " + message.content.split()[3]:
          await message.channel.send("Trombas n vai mais floodar coisas")
      else:
        # i = 2
        # mensagem_say = message.content.split()
        # str_final = ""
        # while i<len(mensagem_say):
        #   str_final+=mensagem_say[i]+" "
        #   i+=1

        await message.channel.send(msg_original.replace(msg_original.split()[0]+" "+msg_original.split()[1], ""))
        #await message.channel.send(message.content.split()[2].replace("_", " "))

    elif "trombas sucumba" == message.content:
      await message.channel.send("+ban <@784497863320404020>")
      await message.channel.send("sucumbirei...")
    elif "cicero" in message.content or "cícero" in message.content:
      await message.channel.send("3,14159265358979323846…")
    elif "trombas omni gif" == message.content:
      await message.channel.send("https://images-ext-2.discordapp.net/external/-5fqRslnUVeVtsjH2lfe_RNVTb2di6UHHAdFDObl9Ro/%3Fwidth%3D167%26height%3D427/https/media.discordapp.net/attachments/874097874890268693/876978709226192926/omni_ass.gif?width=141&height=360")
    elif "trombas omni" == message.content:
      await message.channel.send("https://tenor.com/view/invincible-omniman-twerk-thicc-booty-gif-22139528")
    elif "trombas motiva" == message.content:
      await message.channel.send("https://media.discordapp.net/attachments/818231549987520533/878031842320719872/unknown-11.png")
    elif "trombas vacina list" == message.content:
        usuario = ""
        usuario = message.author.name
        resposta =  lista_vacina(str(message.author.id), usuario)
        await message.channel.send(resposta)
    elif "trombas vacina" == message.content.split()[0]+" "+message.content.split()[1]:
        resposta =  vacina_add(str(message.author.id), message.content.split()[2])
        await message.channel.send(resposta)
    elif "trombas trombofobia" == message.content:
        nmr = random()*100
        if message.author.id == 542104380783198218:
            await message.channel.send("vinicius é 200% trombofobico")
        else:
            await message.channel.send("Você é "+str(round(nmr,0))+"% trombofobic")
    elif "trombas trombossexualidade" == message.content:
        nmr = random()*100
        await message.channel.send("Você é "+str(round(nmr,0))+"% trombossexual")
    elif "trombas zeca" == message.content:
        await message.channel.send("https://tenor.com/view/vote-1310-zeca-again-gif-12408716")
    elif "trombas edge" == message.content:
        await message.channel.send("mine https://media.discordapp.net/attachments/751207503697543168/887360074174046308/trombas.gif")

    elif len(message.content.split())>3 and "trombas sorteio add" == message.content.split()[0] + " " +message.content.split()[1] + " " + message.content.split()[2]:
      i = 3
      mensagem_say = message.content.split()
      str_final = ""
      while i<len(mensagem_say):
        if i < len(mensagem_say)-1:
          str_final+=mensagem_say[i]+" "
        else:
          str_final+=mensagem_say[i]
        i+=1
      change_sorteio(str_final.replace(" ","_"))
      await message.channel.send(str_final+" esta na lista de sorteio")
    elif "trombas sortear" == message.content:
      tam = len(sorteio.split())-1
      print(tam)
      nmr = random()*tam
      print(sorteio)
      print(nmr)
      await message.channel.send("A pessoa sorteada foi: "+sorteio.split()[int(nmr)].replace("_"," ")+" :partying_face: :confetti_ball: \nParabens!")
    elif "trombas sorteio clear" == message.content:
      await message.channel.send("Lista de sorteio foi limpa")
      sorteio_clear()
    elif "trombas sorteio list" == message.content:
      nomes = sorteio.replace(" ","\n")
      await message.channel.send("lista de pessoas participando do sorteio:\n"+nomes)
    elif "trombas lero lero" == message.content:
        await message.channel.send(lero_lero())



    if "trombas" in message.content and message.author.id == 542104380783198218:
      vinicius = [1,2,3,4]
      shuffle(vinicius)
      if vinicius[0] == 1:
        await message.channel.send("vinicius boy tome cuidado....")
      elif vinicius[0] == 2:
        await message.channel.send("ei vinicius mermao, pense bem antes de fazer sapora")
      elif vinicius[0] == 3:
        await message.channel.send("como diria minha vó: minino voce vai ver")
      elif vinicius[0] == 4:
        await message.channel.send("quem manda rouba meus kakera?")
        
    if "trombas phineas" == message.content:
      message.channel.send("https://pbs.twimg.com/media/D3A93NzWoAIc0Me.jpg")

    elif "trombas enem" == message.content:
      lel = random()*1000
      await message.channel.send(str(lel))

    elif "trombas fnaf" == message.content.split()[0]+" "+message.content.split()[1]:
      await message.channel.send("Trombas está procurando Fnafs, por favor aguarde...")
      await message.channel.send("Link do trombas fnaf: https://gamejolt.com/games/FNAT/700393")

      keywords = ["chica", "foxy", "balloon boy", "bonnie", "freddy", "feddy", "puppet", "baby", "fnaf", "fnaf 2","fnaf 3","fnaf 4", "fnaf sister location", "fnaf 6", "golden freddy", "phone guy", "william afton", "mangle", "toy chica", "toy bonnie", "funtime freddy", "funtime freddy", "funtime foxy", "hand unit", "lefty"]
      shuffle(keywords)
      if len(message.content.split())>3 and message.content.split()[2] != "" and message.content.split()[2] != None and message.content.split()[2].lower() != "hentai": #sei q vai ter gente q vai tentar fazer isso, ent é melhor prevernir lol
        keywords_ = "FNAF "+message.content.replace(message.content.split()[0]+" "+message.content.split()[1], '')
      else:
        keywords_ = keywords[0]+","+keywords[1]+","+keywords[2]+","+keywords[3]+","+keywords[4]+","+keywords[5]+","+keywords[6]+","+"Fnaf"


      ddg.download(keywords_, max_urls=3, shuffle=True,  folder="Fnafs", remove_folder=True)
      paths = glob.glob("./Fnafs/*.jpg")
      size = os.path.getsize(paths[0])
      if size < 1:
        paths[0] = paths[1]
      print(paths)

      with open(paths[0], 'rb') as fp:
            imagem = discord.File(fp, filename=paths[0], spoiler=False)

      

      await message.channel.send(file=imagem)
    
    elif "trombas genshin" == message.content.split()[0]+" "+message.content.split()[1] or "trombas ganso" == message.content.split()[0]+" "+message.content.split()[1]:
      await message.channel.send("Trombas está procurando gansos, por favor aguarde...")

      keywords = ["genshin impact" , "albedo", "kaeya", "amber", "itto", "barbara", "beidou", "bennet", "Chongyun", "Diluc", "Diona", "Eula", "Fischl", "Ganyu", "Gorou", "Hutao", "Jean", "Kazuha", "xiao", "mona", "update", "shitpost", "zhongli"]
      shuffle(keywords)
      if len(message.content.split())>3 and message.content.split()[2] != "" and message.content.split()[2] != None and message.content.split()[2].lower() != "hentai": #sei q vai ter gente q vai tentar fazer isso, ent é melhor prevernir lol
        keywords_ = "Genshsin "+message.content.replace(message.content.split()[0]+" "+message.content.split()[1], '')
      else:
        keywords_ = keywords[0]+","+keywords[1]+","+keywords[2]+","+keywords[3]+","+keywords[4]+","+keywords[5]+","+keywords[6]+","+"genshin"

      
      ddg.download(keywords_, max_urls=3, shuffle=True,  folder="ganso", remove_folder=True)
      paths = glob.glob("./ganso/*.jpg")
      size = os.path.getsize(paths[0])
      if size < 1:
        paths[0] = paths[1]
      print(paths)

      with open(paths[0], 'rb') as fp:
            imagem = discord.File(fp, filename=paths[0], spoiler=False)
      await message.channel.send(file=imagem)


    elif "trombas search" == message.content.split()[0]+" "+message.content.split()[1]:
      await message.channel.send("Trombas está procurando seja la o que voce pediu, por favor aguarde...")
      size = 0

      keywords_ = message.content.replace(message.content.split()[0]+" "+message.content.split()[1], '')

      ddg.download(keywords_, max_urls=3, shuffle=True,  folder="search", remove_folder=True)
      paths = glob.glob("./search/*.jpg")
      size = os.path.getsize(paths[0])
      if size < 1:
        paths[0] = paths[1]

      print(paths)
      print(paths[0])

      with open(paths[0], 'rb') as fp:
            imagem = discord.File(fp, filename=paths[0], spoiler=False)
      if "hentai" in message.content or "sex" in message.content or "gore" in message.content or "suicid" in message.content or "nude" in message.content or "pelad" in message.content:
        await message.channel.send("nao.")
      else:
        await message.channel.send(file=imagem)
      
    elif "trombas fnat" == message.content or "fnaf trombas" == message.content:
      await message.channel.send("https://gamejolt.com/games/FNAT/700393")
    
    elif "trombas apagar" == message.content.split()[0]+" "+message.content.split()[1]:
      tmp_msg = await message.channel.fetch_message(message.content.split()[2])
      
      await tmp_msg.delete()
      await message.channel.send("Mensagem apagada :)")

    elif "trombas morbius" == message.content:
      morbius_txt = io.open("./morbius.txt", mode="r", encoding="utf-8")
      morbius_ = morbius_txt.read()
      # print(morbius_)
      morbius_ = morbius_.split("\n")
      shuffle(morbius_)
      print(morbius_[0])
      
      while not re.search('[a-zA-Z]', morbius_[0]):
        shuffle(morbius_)
      morbius(morbius_[0])
      with open('Morbius_text.png', 'rb') as fp:
            imagem = discord.File(fp, filename='MORBIS_heart_emoji.png', spoiler=False)
      await message.channel.send(file=imagem)

    elif "trombas morbius text" == message.content.split()[0] + " " + message.content.split()[1] + " "+ message.content.split()[2]:
      txt = message.content.replace("trombas morbius text", "")
      morbius(txt)
      with open('Morbius_text.png', 'rb') as fp:
            imagem = discord.File(fp, filename='MORBIS_heart_emoji.png', spoiler=False)
      await message.channel.send(file=imagem)



#======== COMANDOS FIM =================

#inutil pra fazer um bot de discord, pode ignorar
keep_alive.keep_alive()


# Todos os bots de discord tem seu proprio token
# algo que eles usam pra terem a permissão de enviarem
# e receberem mensagens, então para que ele possa rodar, é 
# necessário informar qual é o seu token, para que o discord
# possa reconhece-lo e autoriza-lo a realizar essas tarefas
# 
# Nesse caso, pedimos para que o usuario forneça o token
# do bot, armazene o token na variavel x, e rode o bot usando esse
# token

x = input("Digite a token do bot: ")
client.run(x)

# Então basicamente um bot de discord tem 3 etapas
# 1) importar a biblioteca e definir um cliente 
# 2) ler e interpretar as mensagens
# 3) rodar o bot com seu token certo


